<?php

class Building{

	// Access Modifiers
	// This are keywords that can be used to control the "visibility" of properties and methods in a class.

	// 1. public: fully open, properties or methods can be accessed from everywhere.
	// 2. private: properties or methods can only be accessed within the class and disables inheritances.
	// 3. protected: properties or methods is only accessible within the class and on its child class.

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function getName(){
		return $this->name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		$this->name = $name;
	}

	private function setFloors($name){
		$this->floors = $floors;
	}

	private function setAddress($name){
		$this->address = $address;
	}
}

class Condominium extends Building{
	// "Encapsulation" indicates that data must not be directly accessible to users, but can be access only through public functions (setter and getter function)

	// Getters and Setters
		// This are used to retrieve and modify values of each property of the object.
		// Each property of an object should have a set of getter and setter function.

	// getter - accessors
	// This is use to retrieve/access the value of an instantiated object.
	public function getName(){
		return $this->name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	// setter - mutators
	// This is use to change/modify the default value of a property of an instantiated object.
	public function setName($name){
		$this->name = $name;

		// setter functions can also be modified to add data validations.
		// if(gettype($name) === "string"){
		// 	$this->name = $name;
		// }
	}

	public function setFloors($name){
		$this->floors = $floors;
	}

	public function setAddress($name){
		$this->address = $address;
	}

}

$building = new Building("Caswynn Building", 8, "Timog Avenue, Quezon City, Philippines");
$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");